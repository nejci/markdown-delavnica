# Markdown - delavnica za hitro prvo uporabo

Narejeno v sklopu projekta PDPPI.

## Viri

- [Vodič za Markdown](https://www.markdownguide.org/cheat-sheet/)
- [Dokumentacija za GitLab Markdown okus](https://docs.gitlab.com/ee/user/markdown.html)
- [Še en priročnik za GitLab Markdown](https://about.gitlab.com/handbook/markdown-guide/)
- [Urejevalnik kode Visual Studio Code](https://code.visualstudio.com/)

## Danes se bomo naučili:

- [x] Oblikovanje besedila
- [x] Vstavljanje povezav
- [x] Vstavljanje slik
- [x] Oblikovanje tabel

## Oblikovanje besedila

- *kurzivno*
- **krepko**
- ***oboje***
- ~~prečrtano~~  
- `dobesedno`  - poševni narekovaj: <kbd>AltGr</kbd>+<kbd>7</kbd>

Kako narediš znak `~`? Pritisneš kombinacijo tipk: <kbd>AltGr</kbd>+<kbd>1</kbd>.

Še ostali znaki, ki jih pogosto potrebujemo:

`~` : <kbd>AltGr</kbd>+<kbd>1</kbd>

`^` : <kbd>AltGr</kbd>+<kbd>3</kbd>

`|` : <kbd>AltGr</kbd>+<kbd>W</kbd>

`\` : <kbd>AltGr</kbd>+<kbd>Q</kbd>

> Navedba
> nekoga
> ali nečesa

Trije pomišljaji `---` naredijo vodoravno črto

---

### Pisanje kode

```python
def f(x, y):
    return x + y
```

Ukazna vrstica:

```bash
cmdock -i input.sdf -o rezultat.sdf -n 5
```

### Čustveni simboli: 

:joy:

:thumbsup:

## Uporaba odprtokodnega programa *CmDock*: vtičnik za *Pymol*

### Kaj je *CmDock*?

*CmDock* je odprtokodni program za molekulsko sidranje ligandov na proteine in nukleinske kisline. Mogoče še kaj za dopisati.

$$ x_1 = \int_0^\infty  x dx$$

### Inštalacija vtičnika _CmDock_ za _Pymol_

😀

Obišči spletno stran [https://gitlab.com/Jukic/cmdock](https://gitlab.com/Jukic/cmdock). Pritisni `Deployments` → `Releases`. Pod naslovom _Binaries_ najdeš datoteko oblike `.zip` za ustrezen operacijski sistem. Datoteko naloži in jo razpakiraj (_Extract Here_) v poljubno mapo. Za Windows: poženi _install_set_path.bat_ v _Ukazni poziv_ (oz. _Command Prompt_) ali dvakrat klikni na `install_set_path.bat` znotraj želene mape.

## Slike


![Logotip CmDock](https://gitlab.com/Jukic/cmdock/-/raw/master/docs/_static/logo-wide.svg)

Takole to vstavimo, uporabimo lahko relativno pot do datoteke. Se pravi, slika je v mapi img ...

![Opis slike](img/Cavity_4IBK.png)

Uporaba značke HTML za vstavljanje slike, pri čemer lahko določimo njeno širino, npr. 100 px:

`<img src="https://gitlab.com/Jukic/cmdock/-/raw/master/docs/_static/logo-wide.svg" width="100" />`

<img src="https://gitlab.com/Jukic/cmdock/-/raw/master/docs/_static/logo-wide.svg" width="100" />

| ime   | priimek   | letnik |
|----:  | -------   | :----: |
| Ivana | Podlipnik | 2      |
| A     | K         | 2      |
